
Introduction
============

This is the **Angular** version of **AdminLTE** -- what is a fully responsive admin template. Based on **[Bootstrap 3](https://github.com/twbs/bootstrap)** framework. Highly customizable and easy to use. Fits many screen resolutions from small mobile devices to large desktops. Check out the live preview now and see for yourself.

For more AdminLTE information visit  [AdminLTE.IO](https://adminlte.io/)

Installation
------------

```bash
npm install -g @angular/cli
```
- Clone the repository


- Install the packages
```
npm install
```

Running the application
------------
- On the folder project
```
ng serve
```
- For starter page Navigate to [http://localhost:4200/](http://localhost:4200/)
- For admin page Navigate to [http://localhost:4200/admin](http://localhost:4200/admin)

Browser Support
---------------
- IE 9+
- Firefox (latest)
- Chrome (latest)
- Safari (latest)
- Opera (latest)

License
-------
Angular2-AdminLTE is an open source project by that is licensed under [MIT](http://opensource.org/licenses/MIT).

 Credits
-------------
[AdminLTE.IO](https://adminlte.io/)

